// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef H13_H13GameModeBase_generated_h
#error "H13GameModeBase.generated.h already included, missing '#pragma once' in H13GameModeBase.h"
#endif
#define H13_H13GameModeBase_generated_h

#define H13_Source_H13_H13GameModeBase_h_15_RPC_WRAPPERS
#define H13_Source_H13_H13GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define H13_Source_H13_H13GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAH13GameModeBase(); \
	friend struct Z_Construct_UClass_AH13GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AH13GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/H13"), NO_API) \
	DECLARE_SERIALIZER(AH13GameModeBase)


#define H13_Source_H13_H13GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAH13GameModeBase(); \
	friend struct Z_Construct_UClass_AH13GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AH13GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/H13"), NO_API) \
	DECLARE_SERIALIZER(AH13GameModeBase)


#define H13_Source_H13_H13GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AH13GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AH13GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AH13GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AH13GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AH13GameModeBase(AH13GameModeBase&&); \
	NO_API AH13GameModeBase(const AH13GameModeBase&); \
public:


#define H13_Source_H13_H13GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AH13GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AH13GameModeBase(AH13GameModeBase&&); \
	NO_API AH13GameModeBase(const AH13GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AH13GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AH13GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AH13GameModeBase)


#define H13_Source_H13_H13GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define H13_Source_H13_H13GameModeBase_h_12_PROLOG
#define H13_Source_H13_H13GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	H13_Source_H13_H13GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	H13_Source_H13_H13GameModeBase_h_15_RPC_WRAPPERS \
	H13_Source_H13_H13GameModeBase_h_15_INCLASS \
	H13_Source_H13_H13GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define H13_Source_H13_H13GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	H13_Source_H13_H13GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	H13_Source_H13_H13GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	H13_Source_H13_H13GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	H13_Source_H13_H13GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> H13_API UClass* StaticClass<class AH13GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID H13_Source_H13_H13GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
